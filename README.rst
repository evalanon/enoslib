EnosLib
=======

|Build Status| |License| |Pypi|

Join us on gitter :  |Gitter|

EnOSlib helps you to drive your experiments on various infrastructures. To do
so, most of your experiment logic is made *reusable* by EnOSlib (getting
resources, launching remote actions, deploying side analysis stacks ...)

More pragmatically, with EnOSlib, you can iterate on your application
deployment and experimental workflow locally before moving to a large testbed
like Grid'5000, or Chameleon. It saves time and energy.

EnOSlib is designed for experimentation purpose: benchmark in a controlled
environment, academic validation ...

EnOSLib is developed in the context of the
`Discovery <https://beyondtheclouds.github.io/>`_ initiative

.. |Build Status| image:: https://gitlab.inria.fr/discovery/enoslib/badges/master/pipeline.svg
   :target: https://gitlab.inria.fr/discovery/enoslib/pipelines

.. |License| image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0

.. |Pypi| image:: https://badge.fury.io/py/enoslib.svg
   :target: https://badge.fury.io/py/enoslib

.. |Gitter| image:: https://badges.gitter.im/BeyondTheClouds/enoslib.svg
   :alt: Join the chat at https://gitter.im/BeyondTheClouds/enoslib
   :target: https://gitter.im/BeyondTheClouds/enoslib?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge

.. |Coverage| image:: https://gitlab.inria.fr/discovery/enoslib/badges/master/coverage.svg
   :target: https://sonarqube.inria.fr/sonarqube/dashboard?id=discovery%3Aenoslib%3Adev
